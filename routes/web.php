<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// login
Route::get('/login', 'LoginController@index')->name('login');

Route::get('/', function () {
    return view('home');
});

//admin
Route::get('/admin', 'AdminController@index')->name('admin'); //belum pakai id user

// teknisi
Route::get('/tech', 'TechnicianController@index')->name('tech'); //belum pakai id user

// finance
Route::get('/finance', 'FinanceController@index')->name('finance'); //belum pakai id user

//manager
Route::get('/manager', 'ManagerController@index')->name('manager'); //belum pakai id user

// Route::get('/', function () {
//     return view('welcome');
// });
