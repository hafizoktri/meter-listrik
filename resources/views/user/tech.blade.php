@extends('index')

@section('content')
    <div class="container">
        <div>
            <h3>
                Selamat Datang "user" sebagai "teknisi"
            </h3>
        </div>

        <section class="border rounded my-5">
            <form action="" method="post" class="p-3">
                <h5>Masukkan input data dibawah ini dengan benar</h5>

                <p>Meter Besar PLN</p>

                <div class="form-group pb-3">
                    <label for="">Bulan</label>
                    <select name="" id="" class="form-control">
                        <option value="1">Januari</option>
                        <option value="2">Februari</option>
                        <option value="3">Maret</option>
                        <option value="4">April</option>
                        <option value="5">Mei</option>
                        <option value="6">Juni</option>
                        <option value="7">Juli</option>
                        <option value="8">Agustus</option>
                        <option value="9">September</option>
                        <option value="10">Oktober</option>
                        <option value="11">November</option>
                        <option value="12">Desember</option>
                    </select>
                </div>

                <div class="form-group pb-3">
                    <label for="lwbp1">LWBP Awal</label>
                    <input class="form-control" type="text" id="lwbp1" name="lwbp1">
                </div>
    
                <div class="form-group pb-3">
                    <label for="lwbp2">LWBP Akhir</label>
                    <input class="form-control" type="text" id="lwbp2" name="lwbp2">
                </div>
    
                <div class="form-group pb-3">
                    <label for="wbp1">WBP Awal</label>
                    <input class="form-control" type="text" id="wbp1" name="wbp1">
                </div>
    
                <div class="form-group pb-3">
                    <label for="wbp2">WBP Akhir</label>
                    <input class="form-control" type="text" id="wbp2" name="wbp2">
                </div>
    
                <div class="form-group pb-3">
                    <label for="lokasi1">Lokasi</label>
                    <input class="form-control" type="text" id="lokasi1" name="lokasi1">
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-success">Save</button>
                </div>
            </form>

        </section>

        <section class="border rounded my-5">
            <form action="" method="post" class="p-3">
                <p>Meter PLN Tenant</p>
                
                <div class="form-group pb-3">
                    <label for="">Bulan</label>
                    <select name="" id="" class="form-control">
                        <option value="1">Januari</option>
                        <option value="2">Februari</option>
                        <option value="3">Maret</option>
                        <option value="4">April</option>
                        <option value="5">Mei</option>
                        <option value="6">Juni</option>
                        <option value="7">Juli</option>
                        <option value="8">Agustus</option>
                        <option value="9">September</option>
                        <option value="10">Oktober</option>
                        <option value="11">November</option>
                        <option value="12">Desember</option>
                    </select>
                </div>

                <div class="form-group pb-3">
                    <label for="lwbp3">LWBP Awal</label>
                    <input class="form-control" type="text" id="lwbp3" name="lwbp3">
                </div>
    
                <div class="form-group pb-3">
                    <label for="lwbp4">LWBP Akhir</label>
                    <input class="form-control" type="text" id="lwbp4" name="lwbp4">
                </div>
    
                <div class="form-group pb-3">
                    <label for="wbp3">WBP Awal</label>
                    <input class="form-control" type="text" id="wbp3" name="wbp3">
                </div>
    
                <div class="form-group pb-3">
                    <label for="wbp4">WBP Akhir</label>
                    <input class="form-control" type="text" id="wbp4" name="wbp4">
                </div>

                <div class="form-group pb-3">
                    <label for="toko1">Nama Toko Tenant</label>
                    <input class="form-control" type="text" id="toko1" name="toko1">
                </div>
    
                <div class="form-group pb-3">
                    <label for="lokasi2">Lokasi</label>
                    <input class="form-control" type="text" id="lokasi2" name="lokasi2">
                </div>

                <div class="form-group pb-3">
                    <label for="noloc">No Lokasi</label>
                    <input class="form-control" type="text" id="noloc" No name="noloc">
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-success">Save</button>
                </div>
            </form>
        </section>

    </div>
@endsection