@extends('index')

@section('content')
<div class="container">
    <h3>
        Selamat Datang "user" sebagai "manager"
    </h3>

    <section class="border rounded my-5">
        <form action="" method="post" class="p-3">
            <h5>Aplikasi catat meter PLN</h5>

            <div class="form-group pb-3">
                <label for="">Bulan</label>
                <select name="" id="" class="form-control">
                    <option value="1">Januari</option>
                    <option value="2">Februari</option>
                    <option value="3">Maret</option>
                    <option value="4">April</option>
                    <option value="5">Mei</option>
                    <option value="6">Juni</option>
                    <option value="7">Juli</option>
                    <option value="8">Agustus</option>
                    <option value="9">September</option>
                    <option value="10">Oktober</option>
                    <option value="11">November</option>
                    <option value="12">Desember</option>
                </select>
            </div>

            <div class="form-group pb-3">
                <label for="pln">No Meter PLN Besar</label>
                <input class="form-control" type="text" id="pln" name="pln">
            </div>
            
            <p>meter besar PLN</p>

            <div class="form-group pb-3">
                <label for="location">Lokasi/Posisi</label>
                <input class="form-control" type="text" id="location" name="location">
            </div>

            <div class="form-group pb-3">
                <label for="kwh_lwbp">Pemakaian kwh LWBP</label>
                <input class="form-control" type="text" id="kwh_lwbp" name="kwh_lwbp">
            </div>

            <div class="form-group pb-3">
                <label for="kwh_wbp">Pemakaian kwh WBP</label>
                <input class="form-control" type="text" id="kwh_wbp" name="kwh_wbp">
            </div>

            <div class="form-group pb-3">
                <label for="cost">Biaya Pemakaian</label>
                <input class="form-control" type="number" id="cost" name="cost">
            </div>

            <div class="form-group pb-3">
                <label for="price_lwbp">Harga kwh LWBP</label>
                <input class="form-control" type="number" id="price_lwbp" name="price_lwbp">
            </div>

            <div class="form-group pb-3">
                <label for="price_wbp">Harga kwh WBP</label>
                <input class="form-control" type="number" id="price_wbp" name="price_wbp">
            </div>
            
            <div class="form-group">
                <button type="submit" class="btn btn-success">Save</button>
            </div>
        </form>
    </section>
</div>
@endsection