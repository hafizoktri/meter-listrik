@extends('index')

@section('content')
    <div class="container p-5">
        <h4 class="text-center align-items-center justify-content-center">
            selamat datang di <br>
            aplikasi catat meteran listrik
        </h4>

        <div class="text-center">
            <a href="{{ route('login') }}" class="btn btn-primary">LOGIN</a>
        </div>

        <a hidden href="{{route('tech')}}">/tech</a> <br>
        <a hidden href="{{route('finance')}}">/finance</a> <br>
        <a hidden href="{{route('manager')}}">/manager</a> <br>
        <a hidden href="{{route('admin')}}">/admin</a>
    </div>

@endsection